const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const studentRouter = require('./students/student.router');

const PORT = 3000;

const app = express();

app.use(cors());
// app.use(express.json());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use('/student', studentRouter);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}.`)
});