const express = require('express');
const StudentController = require('./student.controller');

const studentRouter = new express.Router();

studentRouter.get('/', StudentController.getStudents);

studentRouter.post('/', StudentController.createStudent);

studentRouter.put('/:studentId', StudentController.updateStudentById);

studentRouter.delete('/:studentId', StudentController.deleteStudentById);

module.exports = studentRouter;
