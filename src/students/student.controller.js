const knex = require('../db');

async function getStudents(req, res) {
  // Retrieve the id, first names, last names, birth dates, and enrolled dates
  const students = await knex('students')
    .select('id', 'first_name', 'last_name', 'birth_date', 'enrolled_date');

  return res.json(students);
}

async function createStudent(req, res) {
  // Assign the new student's information in the request body to a variable.
  const newStudent = req.body;

  // Insert the new student information into the database, retrieving the new id, first name,
  // last name, birth date, and enrolled date.

  // This returns a single-length array containing the new record's id.
  const idArray = await knex('students').insert({
    first_name: newStudent.first_name,
    last_name: newStudent.last_name,
    birth_date: newStudent.birth_date,
    enrolled_date: newStudent.enrolled_date,
  });

  const newId = idArray[0];
  const response = await knex('students')
    .select('id', 'first_name', 'last_name', 'birth_date', 'enrolled_date')
    .where({ id: newId })
    .first()

  console.log(response);

  // Send the new student information, now with an ID, back to the client.
  return res.json(response);
}

async function updateStudentById(req, res) {
  // TODO
  throw new Error('not implemented');
}

async function deleteStudentById(req, res) {
  // TOOD
  throw new Error('not implemented');
}

module.exports = {
  getStudents,
  createStudent,
  updateStudentById,
  deleteStudentById,
};