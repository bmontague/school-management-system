
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('instructors').del()
    .then(function () {
      const ts = new Date();

      // Inserts seed entries
      return knex('instructors').insert([

        {
          first_name: 'Ashley',
          last_name: 'Davis',
          birth_date: '1981-07-12',
          hired_date: '2004-03-20',
          created_at: ts,
          updated_at: ts,
        },
        {
          first_name: 'Jessica',
          last_name: 'Adams',
          birth_date: '1974-05-08',
          hired_date: '2018-03-02',
          created_at: ts,
          updated_at: ts,
        },
        {
          first_name: 'Samantha',
          last_name: 'Young',
          birth_date: '1990-09-23',
          hired_date: '2015-01-14',
          created_at: ts,
          updated_at: ts,
        },
        {
          first_name: 'Stacy',
          last_name: 'Barnes',
          birth_date: '1987-08-09',
          hired_date: '2016-04-21',
          created_at: ts,
          updated_at: ts,
        },
      ]);
    });
};
