
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('students').del()
    .then(function () {
      const ts = new Date();

      // Inserts seed entries
      return knex('students').insert([
        {
          first_name: 'John',
          last_name: 'Doe',
          birth_date: '1998-03-20',
          enrolled_date: '2020-07-12',
          created_at: ts,
          updated_at: ts,
        },
        {
          first_name: 'Chris',
          last_name: 'Smith',
          birth_date: '1992-05-08',
          enrolled_date: '2021-03-02',
          created_at: ts,
          updated_at: ts,
        },
        {
          first_name: 'Steve',
          last_name: 'Jackson',
          birth_date: '1996-09-23',
          enrolled_date: '2018-01-14',
          created_at: ts,
          updated_at: ts,
        },
        {
          first_name: 'Charles',
          last_name: 'Jones',
          birth_date: '1993-08-09',
          enrolled_date: '2019-04-21',
          created_at: ts,
          updated_at: ts,
        },
      ]);
    });
};
