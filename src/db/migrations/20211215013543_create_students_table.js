
exports.up = function(knex) {
    return knex.schema
      .createTable('students', function (table) {
        table.increments('id');
        table.string('first_name', 255).notNullable();
        table.string('last_name', 255).notNullable();
        table.date('birth_date').notNullable();
        table.date('enrolled_date').notNullable();
        table.timestamps();
      });
  };
  
  exports.down = function(knex) {
    return knex.schema
      .dropTable('students');
  };
